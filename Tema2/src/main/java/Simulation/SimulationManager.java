package Simulation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import GUI.SimulatorGUI;
import QueueLogic.Customer;
import QueueLogic.CustomerQueue;
import QueueLogic.SelectionPolicy;
import QueueLogic.StoreScheduler;

public class SimulationManager implements Runnable {

	private int timeLimit; 
	@SuppressWarnings("unused")
	private int maxServiceTime; 
	@SuppressWarnings("unused")
	private int minServiceTime; 
	private int numberOfQueues; 
	@SuppressWarnings("unused")
	private int numberOfClients;  
	private StoreScheduler scheduler;  
	private ArrayList<Customer> generatedCustomers; 	  
	private SimulatorGUI guiManager =  new SimulatorGUI();
	public boolean strg = true;
	
	public void setParamenters(int timeLimit ,int maxServiceTime ,int minServiceTime , int numberOfQueues ,int numberOfClients , SelectionPolicy policy ) {
		this.timeLimit = timeLimit;
		this.maxServiceTime = maxServiceTime;
		this.minServiceTime = minServiceTime;
		this.numberOfQueues = numberOfQueues;
		this.numberOfClients = numberOfClients;
		this.scheduler = new StoreScheduler(numberOfQueues); 
		this.scheduler.changeStrategy(policy);
		this.generatedCustomers = generateCustomers(numberOfClients , timeLimit , minServiceTime , maxServiceTime);
		for(Customer c : this.generatedCustomers) 
			this.scheduler.dispatchCustomer(c); 
	}
	
	public SimulationManager() {
		super(); 
		this.timeLimit = 0;
		this.maxServiceTime = 0;
		this.minServiceTime = 0;
		this.numberOfQueues = 0;
		this.numberOfClients = 0;
		this.scheduler = null; 
		guiManager.addSelectionListener(new StrategyListener()); 
		guiManager.addExecuteListener(new SimulationListener());
		this.generatedCustomers = null;
		
	}
	
	
	public float[] getAverageWatingTime() { 
		float[] out = new float[this.numberOfQueues];
		ArrayList<CustomerQueue> list = this.scheduler.getQueues(); 
		int i =0;
		for(CustomerQueue q : list) { 
			float aux = q.getWaitingTime(); 
			float aux2 = q.getQueueLength();
			out[i] = aux/aux2; 
			i++;
		} 
		
		return out;
	} 
	
	public CustomerQueue getMaximumClientQueue() {
		ArrayList<CustomerQueue> list = this.scheduler.getQueues(); 
		CustomerQueue out = new CustomerQueue();  
		int max = -1;
		for(CustomerQueue q : list) {
			if(q.getQueueLength()>max) 
				out = q;
		} 
		return out;
	} 
	
	public CustomerQueue getMaximumWatingTimeQueue() {
		ArrayList<CustomerQueue> list = this.scheduler.getQueues(); 
		CustomerQueue out = new CustomerQueue();  
		int max = -1;
		for(CustomerQueue q : list) {
			if(q.getWaitingTime()>max) 
				out = q;
		} 
		return out;
	} 
	
	public String statistics() {
		String out = "Simulation ended!\nThe statistics are :\n";  
		float [] x = this.getAverageWatingTime();
		for(int i=1;i<=this.numberOfQueues;i++)
			out += "Queue"+i+" has an average waiting time of " + x[i-1]+"\n"; 
		out += "Queue" + this.getMaximumClientQueue().getId() + " had the biggest number of customers ("+this.getMaximumClientQueue().getQueueLength()+")\n";
		out += "Queue" + this.getMaximumWatingTimeQueue().getId() + " had the biggest waiting time ("+ this.getMaximumWatingTimeQueue().getWaitingTime()+")";		
		return out;
	}
	
	public void changeStrategy(SelectionPolicy policy) {
		this.scheduler.changeStrategy(policy);
	}
	
	public static ArrayList<Customer> generateCustomers(int numberOfClients , int timeLimit , int minServiceTime , int maxServiceTime){ 
		ArrayList<Customer> list = new ArrayList<Customer>(); 
		
		for(int i=0;i<numberOfClients;i++) {
			int id = i+1; 
			int arrivingTime = randomArrivingTime(timeLimit); 
			int serviceTime = randomServiceTime(minServiceTime,maxServiceTime); 
			list.add(new Customer(id,arrivingTime,serviceTime));
		} 
		
		Collections.sort(list);
		return list;
	}
	
	private static int randomArrivingTime(int timeLimit){  
		return (int)(Math.random()*(timeLimit-1))+1;
	} 
	
	private static int randomServiceTime(int minServiceTime , int maxServiceTime) {
		double random = 0; 
		do {
			random = Math.random()* maxServiceTime;
		}while((int)random<minServiceTime); 
		
		return (int)random;
	} 
	
	public String getTheNthQueue(int n) {
		for(int i=0;i<this.numberOfQueues;i++) 
			if((i+1)==n) 
				return this.scheduler.getQueues().get(i).toString(); 
		return null;
	}  
	
	public String getCustomerString() {
		String rez = ""; 
		for(Customer c: this.generatedCustomers) {
			rez += "Customer "+c.getId()+" has arriving time "+c.getArrivingTime()+" and service time "+c.getFinishTime()+"\n";
		} 
		return rez;
	}
	
	@SuppressWarnings("static-access")
	public void run() {
		
		String stats = this.statistics();
		this.guiManager.setTextStatistics(this.getCustomerString());
		
		ArrayList<Thread> threadList = new ArrayList<Thread>(); 
		for(int i=0;i<this.numberOfQueues;i++) {
			threadList.add(new Thread(this.scheduler.getQueues().get(i)));
		}  
		

		for(Thread t : threadList) {
			t.start();
		} 
		
		int currentTime = 1; 
		while(currentTime<=this.timeLimit) {  
			 
			this.guiManager.setTextQueues(this.queuesToString());
			try {
				(new Thread()).sleep(1000); 
				currentTime ++;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
		} 
		
		this.guiManager.setTextStatistics(stats);
		
	}  
	
	public String queuesToString() {
		String  result = new String(""); 
		for(int i=1;i<=this.numberOfQueues;i++) { 
			result +=  this.getTheNthQueue(i)+"\n";
		} 
		return result;
	}
	
	public ArrayList<Customer> getGeneratedCustomers(){
		return this.generatedCustomers;
	}
	public static void main(String[]args) { 
		
		@SuppressWarnings("unused")
		SimulationManager s = new SimulationManager();  
		
	} 
	
	class StrategyListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			
			guiManager.updateState(); 
			
			switch(guiManager.getCurrentState()) {
			
			case 0: 
				strg  = true; 
				break; 
			default: 
				strg = false; 
				break;
			}
			
		}
		
	} 
	
	class SimulationListener implements ActionListener{

		public void actionPerformed(ActionEvent e) { 
			try {
			SimulationManager.this.setParamenters(guiManager.getSimulationTime(), guiManager.getMaxST(), guiManager.getMinST(), guiManager.getNumberOfQueues(), guiManager.getNumberOfClients() , strg ? SelectionPolicy.SHORTEST_TIME:SelectionPolicy.SHORTEST_QUEUE );
			Thread tr = new Thread(SimulationManager.this); 
			tr.start();  
			} 
			catch (Exception err) {
				guiManager.showErrorWindow();
			}
		}
		
	} 

} 


