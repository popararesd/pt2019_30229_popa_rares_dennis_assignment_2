package QueueLogic;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class StoreScheduler {
	private BlockingQueue<CustomerQueue> queues; 
	private Strategy strategy;  
	private int numberOfQueues; 

	public StoreScheduler (int numberOfQueues) {
		this.numberOfQueues = numberOfQueues; 
		this.queues = new LinkedBlockingDeque<CustomerQueue>(); 
		for(int i=0;i<this.numberOfQueues;i++) 
			this.queues.add(new CustomerQueue());
		strategy = null;
	}  
	
	
	public void changeStrategy(SelectionPolicy policy) {
		if(policy == SelectionPolicy.SHORTEST_QUEUE) 
			strategy = new ShortQueueStrategy();  
		if(policy == SelectionPolicy.SHORTEST_TIME) 
			strategy = new ShortTimeStrategy();
	} 
	
	public void dispatchCustomer (Customer c) {
		strategy.addCustomer(queues, c);
	} 
	
	public ArrayList<CustomerQueue> getQueues(){
		ArrayList<CustomerQueue> result = new ArrayList<CustomerQueue>(); 
		Object[] list = this.queues.toArray(); 
		for(int i=0;i<list.length;i++) 
			result.add(((CustomerQueue)list[i]));
		return result;
	} 
	
}
