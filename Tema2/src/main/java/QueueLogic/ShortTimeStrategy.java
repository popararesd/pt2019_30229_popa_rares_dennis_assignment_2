package QueueLogic;

import java.util.concurrent.BlockingQueue;

public class ShortTimeStrategy implements Strategy {

	public void addCustomer(BlockingQueue<CustomerQueue> list, Customer c) {
		
		int minTime = 1000000 , position =0; 
		Object[] list2 = list.toArray();
		for(int i=0;i<list2.length;i++) {
			if(((CustomerQueue)list2[i]).getWaitingTime()<minTime) { 
				position=i;  
				minTime = ((CustomerQueue)list2[i]).getWaitingTime();
			} 
		} 
		int i=0;
		for(CustomerQueue q : list) {
			if(i==position) {
				q.addCustomer(c);
			} 
			i++;
		}
	}

}
