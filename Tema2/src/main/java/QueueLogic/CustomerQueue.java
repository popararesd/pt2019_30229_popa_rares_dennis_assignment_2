package QueueLogic;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class CustomerQueue implements Runnable {
	
	private BlockingQueue<Customer> queue; 
	private int waitingTimeQueue = 0;  
	private static int counter = 1; 
	private int queueId; 
	private int elapsedTime = 0; 
	
	public int getWaitingTime() {
		return waitingTimeQueue;
	}
	
	private void updateWatingTime(int time) {
		this.waitingTimeQueue += time;
	}  
 
	
	public int getId() {
		return queueId;
	}

	public CustomerQueue() { 
		super();
		this.queue = new LinkedBlockingDeque<Customer>();
		queueId=counter++; 
		this.elapsedTime = 0;
	}  
	
	public CustomerQueue(Customer c) {
		super(); 
		this.queue = new LinkedBlockingDeque<Customer>(); 
		this.queue.add(c); 
		this.updateWatingTime(c.getFinishTime()); 
		queueId=counter++; 
		this.elapsedTime = 0;
	}

	public CustomerQueue(BlockingQueue<Customer> queue) {
		super();
		this.queue = queue; 
		Object[] list = this.queue.toArray(); 
		
		for(int i=0;i<list.length;i++) { 
			Customer c = (Customer)list[i];
			this.updateWatingTime(c.getFinishTime()); 
		} 
		
		queueId=counter++; 
		this.elapsedTime = 0;
		
	}
	
	public synchronized void addCustomer(Customer c) { 
		this.queue.add(c); 
		this.updateWatingTime(c.getFinishTime()); 
	}  
	
	public BlockingQueue<Customer> deepCopy() {
		BlockingQueue <Customer> copy = new LinkedBlockingDeque<Customer>();  
		
		Object[] list = this.queue.toArray(); 
		
		for(int i =0;i<list.length;i++) 
		{
			copy.add((Customer)list[i]);
		}
		
		return copy;
	} 
	
	public void removeAll() {
		this.queue.removeAll(queue); 
		this.updateWatingTime(-waitingTimeQueue);
	}
	
	public synchronized Customer removeFirst() {
		
		Customer c = this.queue.poll(); 
		this.updateWatingTime(-c.getFinishTime()); 
		return c; 
		
		
	} 
	
	public int getQueueLength() { 
		 
		if(this.queue.isEmpty()) 
			return 0;
		return this.queue.size(); 
		
	} 
	
	public int getFirstServiceTime() {
		if(this.queue.peek() == null) 
			return 0; 
		else 
			return this.queue.peek().getFinishTime();
	}
	
	public int getElapsedTime() {
		return this.elapsedTime;
	}
	
	@SuppressWarnings("static-access")
	public void run() { 
		try {
			while(!this.queue.isEmpty()) {  
				(new Thread()).sleep(this.getFirstServiceTime()*1000); 
				this.removeFirst();
				this.elapsedTime += this.getFirstServiceTime();
			}  
		
		} 
		catch (InterruptedException e) {
				
		} 
	} 
	
	public String toString() {
		String result = "Queue" + this.getId()+":"; 
		
		for(Customer c : queue) {
				result += c.getId()+",";
		}
		
		return result;
		
	}
	
	public static void main(String[] args) 
	{ 
		CustomerQueue lista = new CustomerQueue();  
		lista.addCustomer(new Customer(1, 1, 1)); 
		lista.addCustomer(new Customer(2, 0, 5)); 
		lista.addCustomer(new Customer(3, 3, 1)); 
		
		CustomerQueue lista2 = new CustomerQueue();  
		lista2.addCustomer(new Customer(4, 1, 1)); 
		lista2.addCustomer(new Customer(5, 0, 5)); 
		lista2.addCustomer(new Customer(6, 3, 1));  
		BlockingQueue<Customer> aux = lista2.deepCopy(); 
		CustomerQueue lista3 = new CustomerQueue(aux);
		lista2.removeAll(); 
		
		
		(new Thread(lista)).start(); 
		(new Thread(lista3)).start();	
	}
	
}
