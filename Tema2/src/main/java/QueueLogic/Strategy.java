package QueueLogic;

import java.util.concurrent.BlockingQueue;

public interface Strategy {
	public void addCustomer(BlockingQueue<CustomerQueue> list , Customer c);
}
