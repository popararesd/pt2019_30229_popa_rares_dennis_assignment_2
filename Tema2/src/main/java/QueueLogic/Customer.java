package QueueLogic;

public class Customer implements Comparable <Customer> {

	private int id;
	private int arrivingTime;
	private int finishTime;
	
	public Customer(int id, int arrivingTime, int finishTime) {
		super();
		this.id = id;
		this.arrivingTime = arrivingTime;
		this.finishTime = finishTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArrivingTime() {
		return arrivingTime;
	}

	public void setArrivingTime(int arrivingTime) {
		this.arrivingTime = arrivingTime;
	}

	public int getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}

	public int compareTo(Customer o) {
		return Integer.compare(this.arrivingTime, o.arrivingTime);
	} 
	
	public String toString() {
		return "Customer"+this.id+" has arriving time " + this.arrivingTime + " and has service time " + this.finishTime;
	}
	
 
	
}
