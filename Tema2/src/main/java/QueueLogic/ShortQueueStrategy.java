package QueueLogic;

import java.util.concurrent.BlockingQueue;

public class ShortQueueStrategy implements Strategy {

	public void addCustomer(BlockingQueue<CustomerQueue> list, Customer c) {
		
		int position = 0, minLength = 1000000 , i =0; 
		
		for(CustomerQueue q : list) {
			if(q.getQueueLength()<minLength) {
				position = i; 
				minLength = q.getQueueLength();
			} 
			i++;
		} 
		i=0;
		for(CustomerQueue q : list) {
			if(i==position) {
				q.addCustomer(c); 
				break;
			} 
			i++;
		}
		
		
	}

}
