package GUI;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class SimulatorGUI extends JFrame {
		
		private int width=1250;
		private int height=600; 
		private JTextField tfNumberOfClients = new JTextField(30); 
		private JTextField tfNumberOfQueues= new JTextField(30); 
		private JTextField tfSimulationTime= new JTextField(30); 
		private JTextField tfMinServiceTime= new JTextField(30); 
		private JTextField tfMaxServiceTime= new JTextField(30); 
		private JComboBox<String> cbStrategy = new JComboBox<String>(new String []{"Shortest Time","Shortest Queue"});
		private JTextArea taQueuesSim = new JTextArea("Queue Simulator Area"); 
		private JTextArea taStatistics = new JTextArea("Customer info/Statistics"); 
		private JButton goBtn = new JButton("START!"); 
		private int state;
		public SimulatorGUI () { 
			
	         JPanel variablesPanel = new JPanel();  
	         variablesPanel.setLayout(new GridLayout(6,4)); 
	         
	         JPanel[][] paneHolder = new JPanel[6][4]; 
	         
	         for(int i=0;i<6;i++) 
	        	 for(int j=0;j<4;j++) {
	        		 paneHolder[i][j] = new JPanel(); 
	        		 variablesPanel.add(paneHolder[i][j]);
	        	 } 
	         
	         
	         JLabel l1 = new JLabel("Introduce the number of clients:"); 
	         JLabel l2 = new JLabel("Introduce the number of queues:"); 
	         JLabel l3 = new JLabel("Introduce the simulation time:"); 
	         JLabel l4 = new JLabel("Introduce the min. service time:"); 
	         JLabel l5 = new JLabel("Introduce the max. service time:"); 
	         JLabel l6 = new JLabel("Choose the strategy:");
	         paneHolder[0][1].add(l1); 
	         paneHolder[1][1].add(l2);  
	         paneHolder[2][1].add(l3); 
	         paneHolder[3][1].add(l4); 
	         paneHolder[4][1].add(l5); 
	         paneHolder[5][1].add(l6); 
	         
	         paneHolder[0][2].add(this.tfNumberOfClients); 
	         paneHolder[1][2].add(this.tfNumberOfQueues);  
	         paneHolder[2][2].add(this.tfSimulationTime); 
	         paneHolder[3][2].add(this.tfMinServiceTime); 
	         paneHolder[4][2].add(this.tfMaxServiceTime); 
	         paneHolder[5][2].add(this.cbStrategy);
	         
	         paneHolder[2][3].add(this.goBtn);
	         JPanel queuesPanel = new JPanel(); 
	         JPanel statisticsPanel = new JPanel(); 
	         variablesPanel.setBackground(Color.WHITE); 
	         queuesPanel.setBackground(Color.WHITE); 
	         statisticsPanel.setBackground(Color.WHITE); 
	         queuesPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1)); 
	         statisticsPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1)); 
	         queuesPanel.add(this.taQueuesSim); 
	         statisticsPanel.add(this.taStatistics);
	         Container twoPanelContainer = new JPanel(new GridLayout(1,2)); 
	         twoPanelContainer.add(queuesPanel); 
	         twoPanelContainer.add(statisticsPanel);  
	         JPanel contentPane = new JPanel(new GridLayout(2,1)); 
	         contentPane.add(variablesPanel); 
	         contentPane.add(twoPanelContainer); 
	         this.setContentPane(contentPane);
	         this.setSize(this.width,this.height); 
			 this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
			 this.setVisible(true); 
			 this.state = this.cbStrategy.getSelectedIndex();
	         this.setTitle("Queue Simulator"); 
	        
		} 
		
		public void updateState() {
			this.state = this.cbStrategy.getSelectedIndex();
		} 
		
		public int getCurrentState() {
			return this.state;
		}
		
		public int getNumberOfClients() { 
			return Integer.parseInt(this.tfNumberOfClients.getText()); 
		} 
		
		public int getNumberOfQueues() { 
			return Integer.parseInt(this.tfNumberOfQueues.getText()); 
		} 
		
		public int getMinST() { 
			return Integer.parseInt(this.tfMinServiceTime.getText()); 
		} 
		
		public int getMaxST() { 
			return Integer.parseInt(this.tfMaxServiceTime.getText()); 
		} 
		
		public int getSimulationTime() { 
			return Integer.parseInt(this.tfSimulationTime.getText()); 
		} 
		
		public void setTextQueues(String s) { 
			this.taQueuesSim.setText(s);
		} 
		
		public void setTextStatistics(String s) {
			this.taStatistics.setText(s);
		} 
		
		public void addExecuteListener(ActionListener exe) {
			this.goBtn.addActionListener(exe);
		}

		public void addSelectionListener(ActionListener sel) {
			this.cbStrategy.addActionListener(sel);
		}
		public void closeFrame() {
			this.dispose();
		} 
		
		public void showErrorWindow() {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,"Unexpected error!", "Error", JOptionPane.ERROR_MESSAGE);
		}
}
